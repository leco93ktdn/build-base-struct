import 'package:flutter/material.dart';

import 'colors.dart';

final ThemeData appTheme = ThemeData(
  backgroundColor: Colors.white,
  canvasColor: backgroundTwoColor,
  primaryColor: primaryColor,
  accentColor: accentOneColor,
  appBarTheme: AppBarTheme(
    elevation: 0,
    color: backgroundTwoColor,
    centerTitle: true,
    textTheme: TextTheme(
      headline6: bodyOne,
    ),
    iconTheme: const IconThemeData(
      color: accentOneColor,
    ),
  ),
  fontFamily: 'Mulish',
  buttonTheme: const ButtonThemeData(
    buttonColor: primaryColor,
  ),
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
    backgroundColor: backgroundTwoColor,
    selectedIconTheme: IconThemeData(color: primaryColor, size: 16),
    selectedLabelStyle: bodySevActived,
    selectedItemColor: primaryColor,
    showUnselectedLabels: true,
    showSelectedLabels: true,
    unselectedIconTheme: IconThemeData(color: neutralThreeColor, size: 16),
    unselectedItemColor: neutralThreeColor,
    unselectedLabelStyle: bodySev,
  ),
);

const TextStyle textDef = TextStyle(
  fontSize: 12,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.normal,
  fontStyle: FontStyle.normal,
);

//Title
const TextStyle titleOne = TextStyle(
  fontSize: 22,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.bold,
  fontStyle: FontStyle.normal,
);

const TextStyle titleTwo = TextStyle(
  fontSize: 20,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.bold,
  fontStyle: FontStyle.normal,
);

//Body
const TextStyle bodyOne = TextStyle(
  fontSize: 16,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.bold,
  fontStyle: FontStyle.normal,
);

const TextStyle bodyTwo = TextStyle(
  fontSize: 16,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.w600,
  fontStyle: FontStyle.normal,
);

const TextStyle bodyThree = TextStyle(
  fontSize: 14,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.w400,
  fontStyle: FontStyle.normal,
);

const TextStyle bodyFour = TextStyle(
  fontSize: 14,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.bold,
  fontStyle: FontStyle.normal,
);

const TextStyle bodyFourBold = TextStyle(
  fontSize: 14,
  fontFamily: 'Mulish',
  color: neutralFiveColor,
  fontWeight: FontWeight.bold,
  fontStyle: FontStyle.normal,
);

const TextStyle bodyFive = TextStyle(
  fontSize: 14,
  fontFamily: 'Muli',
  color: neutralOneColor,
  fontWeight: FontWeight.normal,
  fontStyle: FontStyle.normal,
);

const TextStyle bodySix = TextStyle(
  fontSize: 14,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.normal,
  fontStyle: FontStyle.normal,
);

const TextStyle bodySev = TextStyle(
  fontSize: 14,
  fontFamily: 'Mulish',
  color: neutralFourColor,
  fontWeight: FontWeight.normal,
  fontStyle: FontStyle.normal,
);

const TextStyle bodySevActived = TextStyle(
  fontSize: 14,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.normal,
  fontStyle: FontStyle.normal,
);

//Caption
const TextStyle captionOne = TextStyle(
  fontSize: 12,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.bold,
  fontStyle: FontStyle.normal,
);

const TextStyle captionTwo = TextStyle(
  fontSize: 12,
  fontFamily: 'Muli',
  color: neutralOneColor,
);

const TextStyle captionThree = TextStyle(
  fontSize: 8,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.normal,
  fontStyle: FontStyle.normal,
);

const TextStyle captionFour = TextStyle(
  fontSize: 10,
  fontFamily: 'Muli',
  color: neutralOneColor,
);

const TextStyle captionFive = TextStyle(
  fontSize: 8,
  fontFamily: 'Mulish',
  color: neutralOneColor,
  fontWeight: FontWeight.bold,
  fontStyle: FontStyle.normal,
);
