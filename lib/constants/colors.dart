import 'package:flutter/material.dart';

const Color primaryColor = Color(0xFF007936);
const Color boxColor = Color(0xFFF0F1F5);
const Color purpleColor = Color(0xFF75759E);
const Color textColor = Color(0xFF5B6373);

const Color accentOneColor = Color(0xFF364259);
const Color accentTwoColor = Color(0xFFF58220);
const Color accentThreeColor = Color(0xFF005BAA);
const Color accentFourColor = Color(0xFFE51937);

const Color semanticOneColor = Color(0xFFF7BE0C);
const Color semanticTwoColor = Color(0xFFFFFE4E);
const Color semanticThreeColor = Color(0xFF0AB8B6);

const Color neutralOneColor = Color(0xFF333333);
const Color neutralTwoColor = Color(0xFF666666);
const Color neutralThreeColor = Color(0xFF999999);
const Color neutralFourColor = Color(0xFFFFFFFF);
const Color neutralFiveColor = Color(0xFFC4C4C4);

const Color messageColor = Color(0xFFF35C56);

const Color backgroundOneColor = Color(0xFFF8F8F8);
const Color backgroundTwoColor = Color(0xFFFFFFFF);
const Color backgroundThreeColor = Color(0xFFF2F2F2);

const Color supportOneColor = Color(0xFFFCF5DC);
const Color supportTwoColor = Color(0xFFDFF2F7);

const LinearGradient gradientOne = LinearGradient(
  colors: <Color>[Color(0xFF00652E), Color(0xFF328D38), Color(0xFF65B643)],
  begin: FractionalOffset.topLeft,
  end: FractionalOffset.bottomRight,
  stops: <double>[0.0, 0.5, 1.0],
  tileMode: TileMode.clamp,
);

const LinearGradient gradientTwo = LinearGradient(
  colors: <Color>[Color(0xFF00652E), Color(0xFF328D38), Color(0xFF65B643)],
  begin: FractionalOffset.bottomCenter,
  end: FractionalOffset.topCenter,
  stops: <double>[0.0, 0.5, 1.0],
  tileMode: TileMode.clamp,
);

const LinearGradient gradienThree = LinearGradient(
  colors: <Color>[Color(0xFF00652E), Color(0xFF328D38), Color(0xFF65B643)],
  begin: FractionalOffset.topRight,
  end: FractionalOffset.bottomLeft,
  stops: <double>[0.04, 0.5, 1.0],
  tileMode: TileMode.clamp,
);

const LinearGradient gradienFour = LinearGradient(
  colors: <Color>[Color(0xFF272459), Color(0xFF55518F)],
  begin: FractionalOffset.topCenter,
  end: FractionalOffset.bottomCenter,
  stops: <double>[0.0, 1.0],
  tileMode: TileMode.clamp,
);
