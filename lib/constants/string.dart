class KeyShared {
  static const String tokenGlobal = 'token_global';
  static const String phoneNumber = 'phone_number';
  static const String authenticationID = 'authenticationid';
  static const String userID = 'userid';
  static const String authenticationKey = 'authenticationkey';
}


class Strings {
  //dashboard
  static const String tabOne = 'Trang chủ';
  static const String tabTwo = 'Bán hàng';
  static const String tabThree = '';
  static const String tabFour = 'Khách hàng';
  static const String tabFive = 'Tài khoản';


  static const String backPressToast = 'Nhấn 2 lần để thoát';
}
