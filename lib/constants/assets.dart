class AssetsSVG {
  static const String _baseAssets = 'assets/svg/';
  static const String background = _baseAssets + 'background.svg';
  static const String logo = _baseAssets + 'logo.svg';
  static const String ic_account = _baseAssets + 'ic_account.svg';
  static const String ic_box = _baseAssets + 'ic_box.svg';
  static const String ic_calendar_plus = _baseAssets + 'ic_calendar_plus.svg';
  static const String ic_chart = _baseAssets + 'ic_chart.svg';
  static const String ic_clock = _baseAssets + 'ic_clock.svg';
  static const String ic_news = _baseAssets + 'ic_news.svg';
  static const String ic_customer = _baseAssets + 'ic_customer.svg';
  static const String ic_home = _baseAssets + 'ic_home.svg';
  static const String ic_location = _baseAssets + 'ic_location.svg';
  static const String ic_notification = _baseAssets + 'ic_notification.svg';
  static const String ic_product = _baseAssets + 'ic_product.svg';
  static const String ic_profile = _baseAssets + 'ic_profile.svg';
  static const String ic_round_dollar = _baseAssets + 'ic_round_dollar.svg';
  static const String ic_shop_location = _baseAssets + 'ic_shop_location.svg';
  static const String ic_tags_stack = _baseAssets + 'ic_tags_stack.svg';
  static const String ic_search = _baseAssets + 'ic_search.svg';
  static const String ic_chevron_right = _baseAssets + 'ic_chevron_right.svg';
  static const String ic_store = _baseAssets + 'ic_store.svg';
  static const String ic_detail = _baseAssets + 'ic_detail.svg';
  static const String logo_vn = _baseAssets + 'VN.svg';
}

class AssetsPNG {
  static const String _baseAssets = 'assets/png/';
  static const String background = _baseAssets + 'background.png';
  static const String intro_bg = _baseAssets + 'intro_bg.png';
  static const String bg_screen = _baseAssets + 'bg_screen.png';
  static const String logo = _baseAssets + 'logo.png';
  static const String home_header = _baseAssets + 'home_header.png';
  static const String customer_header = _baseAssets + 'customer_header.png';
  static const String customer_detail_header =
      _baseAssets + 'customer_detail_header.png';
  static const String bg_customer_detail =
      _baseAssets + 'bg_customer_detail.png';
}
