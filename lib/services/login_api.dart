
import 'dart:convert';
import 'package:base_struct_redux_app/configs/configs.dart';
import 'package:base_struct_redux_app/modules/auth/entity/login.entity.dart';
import 'package:http/http.dart' as http;

class AuthenAPI {
  static Future<LoginEntity> loginWithPhoneNumber({
    String phoneNumber,
    String deviceID,
    String deviceName,
    String authKey = '',
  }) async {
    final String dataRequest = jsonEncode({
      'phonenumber': phoneNumber,
      'deviceid': deviceID,
      'devicename': deviceName,
      'authenticationkey': authKey,
    });

    final http.Response response = await http.post(
      '${AppConfig.loadFromEnv().restBaseUrl}_Login',
      body: dataRequest,
      headers: <String, String>{
        'Content-Type': 'application/json',
        'x-cdata-authtoken': AppConfig.loadFromEnv().xCdataAuthtoken,
      },
    );

    if (response.statusCode != 200) {
      print('ERROR LOGIN: ' + response.body);
      return null;
    }

    final json = jsonDecode(response.body);
    if (json != null && json['value'] != null) {
      final LoginEntity data = LoginEntity.fromJson(
          json['value'][0] as Map<String, dynamic>);
      return data;
    }

    return null;
  }
}