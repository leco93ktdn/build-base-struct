import 'package:base_struct_redux_app/modules/auth/view/login.view.dart';
import 'package:base_struct_redux_app/modules/auth/view/welcome.view.dart';
import 'package:base_struct_redux_app/modules/dashboard/view/dashboard.view.dart';
import 'package:get/get.dart';
part 'app_routes.dart';

class AppPage {
  static const String INITIAL = Routes.WELCOM;
  static final List<GetPage> routes = <GetPage>[
    GetPage(name: _Paths.WELCOM, page: () => WelcomeView()),
    GetPage(name: _Paths.LOGIN, page: () => LoginView()),
    GetPage(name: _Paths.DASHBOARD, page: () => DashboardView()),
  ];
}
