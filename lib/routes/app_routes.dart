part of 'app_pages.dart';

abstract class Routes {
  static const String WELCOM = _Paths.WELCOM;
  static const String LOGIN = _Paths.LOGIN;
  static const String OTP = _Paths.OTP;
  static const String DASHBOARD = _Paths.DASHBOARD;
}

abstract class _Paths {
  static const String WELCOM = '/welcome';
  static const String LOGIN = '/login';
  static const String OTP = '/otp';
  static const String DASHBOARD = '/dashboard';
}
