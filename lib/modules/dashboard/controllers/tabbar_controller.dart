import 'package:base_struct_redux_app/constants/assets.dart';
import 'package:base_struct_redux_app/constants/string.dart';
import 'package:base_struct_redux_app/modules/dashboard/models/tabbar_item_model.dart';
import 'package:base_struct_redux_app/modules/dashboard/view/dashboard.view.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class TabbarController extends GetxController{
  static TabbarController get to => Get.find();
  TabType tabType = TabType.home;
  DateTime backButtonOnPressed;
  
  final List<TabbarItemModel> tabs = <TabbarItemModel>[
    TabbarItemModel(Strings.tabOne, AssetsSVG.ic_home, TabType.home),
    TabbarItemModel(Strings.tabTwo, AssetsSVG.ic_store, TabType.order),
    TabbarItemModel(Strings.tabThree, AssetsSVG.ic_calendar_plus, TabType.calendar),
    TabbarItemModel(Strings.tabFour, AssetsSVG.ic_customer, TabType.customer),
    TabbarItemModel(Strings.tabFive, AssetsSVG.ic_account, TabType.account),
  ];

  Future<bool> onWillPop() async{
    final DateTime currentTime = DateTime.now();
    final bool backButton = backButtonOnPressed == null || currentTime.difference(backButtonOnPressed)>Duration(seconds: 2);

    if(backButton) {
      backButtonOnPressed = currentTime;
      await Fluttertoast.showToast(msg: Strings.backPressToast);
      return false;
    }

    return true;
  }

  void onChangeTabe(TabType type) {
    tabType = type;
    update();
  }
}