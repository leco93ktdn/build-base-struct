import 'package:base_struct_redux_app/modules/dashboard/view/dashboard.view.dart';
import 'package:equatable/equatable.dart';

class TabbarItemModel extends Equatable {
  final String label;
  final String icon;
  final TabType type;

  const TabbarItemModel(this.label, this.icon, this.type);

  @override
  List<Object> get props =>
      <Object>[
        label, icon, type,
      ];
}