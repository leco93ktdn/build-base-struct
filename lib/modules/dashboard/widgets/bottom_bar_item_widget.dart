import 'package:base_struct_redux_app/constants/themes.dart';
import 'package:base_struct_redux_app/modules/dashboard/models/tabbar_item_model.dart';
import 'package:base_struct_redux_app/modules/dashboard/view/dashboard.view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BottomBarItemWidget extends StatelessWidget {
  final TabbarItemModel tabModel;
  final Color iconColor;
  final Function(TabType) onSelect;

  const BottomBarItemWidget(
      {Key key, this.tabModel, this.iconColor, this.onSelect}) :super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(child: Material(
      type: MaterialType.transparency,
      child: InkWell(
        borderRadius: BorderRadius.circular(50),
        onTap: () => onSelect(tabModel.type),
        child: (tabModel.type == TabType.calendar)
            ? SizedBox()
            : Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SvgPicture.asset(
                tabModel.icon,
                width: 24,
                height: 24,
                color: iconColor,
              ),
              SizedBox(height: 5),
              Text(
                tabModel.label,
                textAlign: TextAlign.center,
                style: textDef.copyWith(color: iconColor),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        ),
      ),
    ));
  }


}