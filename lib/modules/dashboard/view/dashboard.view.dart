import 'package:base_struct_redux_app/constants/assets.dart';
import 'package:base_struct_redux_app/constants/colors.dart';
import 'package:base_struct_redux_app/modules/dashboard/controllers/tabbar_controller.dart';
import 'package:base_struct_redux_app/modules/dashboard/models/tabbar_item_model.dart';
import 'package:base_struct_redux_app/modules/dashboard/widgets/bottom_bar_item_widget.dart';
import 'package:base_struct_redux_app/modules/home/view/home.view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

enum TabType { home, order, calendar, customer, account }

class DashboardView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<TabbarController>(
        init: TabbarController(),
        builder: (TabbarController controller) {
          return WillPopScope(
              onWillPop: controller.onWillPop,
              child: SafeArea(
                top: false,
                bottom: false,
                child: Scaffold(
                  extendBody: true,
                  backgroundColor: backgroundOneColor,
                  body: _buildBodyTab(controller),
                  bottomNavigationBar: _buildButtonTab(controller),
                  floatingActionButton: _buildFloatingButton(),
                  floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
                ),
              ));
        });
  }

  Widget _buildButtonTab(TabbarController controller) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20)),
          boxShadow: [
            BoxShadow(
                spreadRadius: 4,
                blurRadius: 20,
                color: Color.fromRGBO(0, 0, 0, 0.2)),
          ],
        ),
        child: ClipRRect(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      child: BottomAppBar(
        color: backgroundTwoColor,
        notchMargin: 5,
        shape: CircularNotchedRectangle(),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: List<BottomBarItemWidget>.generate(controller.tabs.length,
              (index) {
            final TabbarItemModel _item = controller.tabs[index];
            return BottomBarItemWidget(
              tabModel: _item,
              iconColor: (controller.tabType == _item.type)
                  ? accentOneColor
                  : neutralThreeColor,
              onSelect: controller.onChangeTabe,
            );
          }),
        ),
      ),
    ));
  }

  Widget _buildBodyTab(TabbarController controller) {
    switch (controller.tabType) {
      case TabType.home:
        return HomeView();
      case TabType.order:
        return Container(color: backgroundTwoColor);
      case TabType.customer:
        return Container(color: backgroundTwoColor);
      case TabType.account:
        return Container(color: backgroundTwoColor);
      default:
        return Container(color: backgroundThreeColor);
    }
  }

  Widget _buildFloatingButton() {
    return FloatingActionButton(
      onPressed: () {},
      child: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.all(13),
        decoration:
            BoxDecoration(shape: BoxShape.circle, gradient: gradientTwo),
        child: SvgPicture.asset(
          AssetsSVG.ic_calendar_plus,
          color: neutralFourColor,
        ),
      ),
    );
  }
}
