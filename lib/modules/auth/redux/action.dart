import 'package:base_struct_redux_app/helpers/get_info_device.dart';
import 'package:base_struct_redux_app/helpers/persistent_state.dart';
import 'package:base_struct_redux_app/modules/auth/entity/login.entity.dart';
import 'package:base_struct_redux_app/redux/app.state.dart';
import 'package:base_struct_redux_app/routes/app_pages.dart';
import 'package:base_struct_redux_app/services/login_api.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class SetLoginLoading {
  SetLoginLoading(this.isLoginLoading);

  final bool isLoginLoading;
}

class SetLoginLoginError {
  SetLoginLoginError(this.errorText);

  final String errorText;
}

class SetLoginData {
  SetLoginData({this.phoneNumber, this.data});

  final LoginEntity data;
  final String phoneNumber;
}

class SetLogout {}

ThunkAction<AppState> loginWithPhoneNumber(
  String phoneNumber,
) {
  return (Store<AppState> store) async {
    if (store.state.loginState.isLoginLoading) {
      return;
    }

    try {
      await store.dispatch(SetLoginLoading(true));
      final PersistentState persistor = PersistentState(GetStorage());
      final Map<String, String> deviceInfo = await getDeviceInfo();
      final String rePhoneNumber = phoneNumber.replaceAll(' ', '');
      final LoginEntity data = await AuthenAPI.loginWithPhoneNumber(
        phoneNumber: rePhoneNumber,
        deviceID: deviceInfo['device_id'],
        deviceName: deviceInfo['device_name'],
        authKey: persistor?.authenticationKey ?? '',
      );

      if (data == null) {
        await store.dispatch(SetLoginLoading(false));
        await store
            .dispatch(SetLoginLoginError('Đã xảy ra lỗi, vui lòng thử lại!'));
        return;
      }

      if (data?.errorcode == 1) {
        await store.dispatch(SetLoginLoading(false));
        await store.dispatch(SetLoginLoginError(data.message));
        return;
      }

      if (data?.otprequire == 1) {
        await store
            .dispatch(SetLoginData(phoneNumber: phoneNumber, data: data));
        await store.dispatch(SetLoginLoading(false));
        Get.toNamed(Routes.OTP);
        return;
      }

      await store.dispatch(SetLoginData(phoneNumber: phoneNumber, data: data));
      await store.dispatch(SetLoginLoading(false));
      await store.dispatch(SetLoginLoginError(null));
      Get.offAllNamed(Routes.DASHBOARD);
    } catch (error) {
      await store.dispatch(SetLoginLoading(false));
      await store
          .dispatch(SetLoginLoginError('Đã xảy ra lỗi, vui lòng thử lại!'));
      print('ERROR LOGIN: $error');
    }
  };
}
