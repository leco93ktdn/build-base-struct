import 'package:base_struct_redux_app/helpers/persistent_state.dart';
import 'package:base_struct_redux_app/modules/auth/state/login.state.dart';
import 'package:get_storage/get_storage.dart';
import 'package:redux/redux.dart';

import 'action.dart';

final Reducer<LoginState> loginReducer = combineReducers([
  TypedReducer<LoginState, SetLoginLoading>(_setLoginLoading),
  TypedReducer<LoginState, SetLoginLoginError>(_setLoginError),
  TypedReducer<LoginState, SetLoginData>(_setLoginData),
  TypedReducer<LoginState, SetLogout>(_setLogout),
]);

LoginState _setLoginLoading(LoginState state, SetLoginLoading action) {
  return state.copyWith(isLoginLoading: action.isLoginLoading);
}

LoginState _setLoginError(LoginState state, SetLoginLoginError action) {
  return state.copyWith(errorCode: action.errorText);
}

LoginState _setLoginData(LoginState state, SetLoginData action) {
  final PersistentState persistor = PersistentState(GetStorage());
  persistor.setLoginInfo(
      phoneNumber: action.phoneNumber,
      userID: action.data.userid,
      authenticationID: action.data.authenticationid);

  state = state.copyWith(
      errorCode: action.data.message,
      isLoginLoading: false,
      loginEntity: action.data);
  return state;
}

LoginState _setLogout(LoginState state, SetLogout action) {
  final PersistentState persistor = PersistentState(GetStorage());
  persistor.setLogout();
  state = state.logout();
  return state;
}
