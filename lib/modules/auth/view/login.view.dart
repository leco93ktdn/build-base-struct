import 'package:base_struct_redux_app/routes/app_pages.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginView extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          body: Center(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("LOGIN PAGE"),
                FlatButton(
                  color: Colors.green,
                  onPressed: () => Get.toNamed(Routes.DASHBOARD),
                  child: Text("Login"),
                ),
              ],
            ),
          ),
        ));
  }

}