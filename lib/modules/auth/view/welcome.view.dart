import 'package:base_struct_redux_app/routes/app_pages.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WelcomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [ Text("Welcome ThanhSonHoaNong"),
            FlatButton(
              color: Colors.green,
              onPressed: () => Get.toNamed(Routes.LOGIN),
              child: Text("Next"),
            ),
          ],
        ),));
  }
}
