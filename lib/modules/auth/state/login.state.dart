import 'package:base_struct_redux_app/modules/auth/entity/login.entity.dart';
import 'package:equatable/equatable.dart';

class LoginState extends Equatable {
  final bool isLoginLoading;
  final String errorCode;
  final LoginEntity loginEntity;

  const LoginState({
    this.isLoginLoading,
    this.errorCode,
    this.loginEntity,
  });

  factory LoginState.initial() {
    return LoginState(
      isLoginLoading: false,
      errorCode: '',
      loginEntity: null,
    );
  }

  LoginState copyWith({
    bool isLoginLoading,
    String errorCode,
    dynamic loginEntity,
  }) {
    return LoginState(
      isLoginLoading: isLoginLoading ?? this.isLoginLoading,
      errorCode: errorCode ?? this.errorCode,
      loginEntity: loginEntity ?? this.loginEntity,
    );
  }

  LoginState logout() {
    return LoginState(
      isLoginLoading: false,
      errorCode: '',
      loginEntity: null,
    );
  }

  @override
  List<Object> get props =>
      <Object>[
        isLoginLoading,
        errorCode,
        loginEntity,
      ];
}
