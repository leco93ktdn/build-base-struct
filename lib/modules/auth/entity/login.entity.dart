import 'package:equatable/equatable.dart';

class LoginEntity extends Equatable {
  String authenticationid;
  int errorcode;
  String message;
  int otprequire;
  int registerstatus;
  String tokenLogin;
  String userid;

  LoginEntity({
    this.authenticationid,
    this.errorcode,
    this.message,
    this.otprequire,
    this.registerstatus,
    this.tokenLogin,
    this.userid,
  });

  LoginEntity.fromJson(Map<String, dynamic> json) {
    authenticationid = json['authenticationid'] as String;
    errorcode = json['errorcode'] as int;
    message = json['message'] as String;
    otprequire = json['otprequire'] as int;
    registerstatus = json['registerstatus'] as int;
    tokenLogin = json['token'] as String;
    userid = json['userid'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['authenticationid'] = authenticationid;
    data['errorcode'] = errorcode;
    data['message'] = message;
    data['otprequire'] = otprequire;
    data['registerstatus'] = registerstatus;
    data['token'] = tokenLogin;
    data['userid'] = userid;
    return data;
  }

  @override
  List<Object> get props => <Object>[
        authenticationid,
        errorcode,
        message,
        otprequire,
        registerstatus,
        tokenLogin,
        userid,
      ];
}
