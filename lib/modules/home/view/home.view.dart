import 'package:base_struct_redux_app/constants/colors.dart';
import 'package:base_struct_redux_app/modules/home/state/home.state.dart';
import 'package:base_struct_redux_app/redux/app.state.dart';
import 'package:base_struct_redux_app/routes/app_pages.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:get/get.dart';
import 'package:redux/redux.dart';

class _ViewModel extends Equatable {
  final bool isHomeLoading;

  const _ViewModel({
    this.isHomeLoading,
  });

  @override
  List<Object> get props => <Object>[
        isHomeLoading,
      ];
}

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector(
      converter: (Store<AppState> store) {
        final HomeState homeState = store?.state?.homeState;
        return _ViewModel(
          isHomeLoading: homeState?.isHomeLoading,
        );
      },
      builder: (BuildContext context, _ViewModel viewModel) {
        return SafeArea(
            top: false,
            bottom: false,
            child: Scaffold(
              backgroundColor: backgroundOneColor,
              body: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("HOME PAGE"),
                    FlatButton(
                      color: Colors.green,
                      onPressed: () => Get.toNamed(Routes.LOGIN),
                      child: Text("Logout"),
                    ),
                  ],
                ),
              ),
            ));
      },
    );
  }
}
