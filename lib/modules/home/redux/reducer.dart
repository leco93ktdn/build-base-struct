import 'package:base_struct_redux_app/modules/home/state/home.state.dart';
import 'package:redux/redux.dart';
import 'action.dart';

final Reducer<HomeState> homeReducer = combineReducers([
  TypedReducer<HomeState, SetHomeLoading>(_setHomeLoading),
]);

HomeState _setHomeLoading(HomeState state, SetHomeLoading action) {
  return state.copyWith(isHomeLoading: action.isHomeLoading);
}
