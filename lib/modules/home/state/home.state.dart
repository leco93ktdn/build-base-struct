import 'package:equatable/equatable.dart';

class HomeState extends Equatable {
  final bool isHomeLoading;

  const HomeState({
    this.isHomeLoading,
  });

  factory HomeState.initial() {
    return HomeState(
      isHomeLoading: false,
    );
  }

  HomeState copyWith({
    bool isHomeLoading,
  }) {
    return HomeState(
      isHomeLoading: isHomeLoading ?? this.isHomeLoading,
    );
  }

  @override
  List<Object> get props => <Object>[
    isHomeLoading,
  ];
}
