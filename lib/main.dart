import 'dart:async';
import 'dart:developer';
import 'package:base_struct_redux_app/redux/app.state.dart';
import 'package:base_struct_redux_app/redux/store.dart';
import 'package:base_struct_redux_app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'constants/themes.dart';
import 'helpers/logger_utils.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  runZoned<Future<void>>(() async => runApp(BaseStructApp()),
      onError: (dynamic error, dynamic stackTrace) {
    log('Error: $error');
    log('Stack: $stackTrace');
  });
}

class BaseStructApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: GetMaterialApp(
        title: 'ThanhSonHoaNongDemo',
        enableLog: true,
        logWriterCallback: Logger.write,
        debugShowCheckedModeBanner: false,
        theme: appTheme,
        initialRoute: AppPage.INITIAL,
        getPages: AppPage.routes,
        builder: (BuildContext context, Widget child){
          return ResponsiveWrapper.builder(child,
          maxWidth: 1000,
          minWidth: 400,
          defaultScale: true,
          mediaQueryData: MediaQuery.of(context).copyWith(textScaleFactor: 1));
        },
      ),
    );
  }
}
