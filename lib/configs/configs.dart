import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';

class AppConfig {
  AppConfig({
    @required this.debugMode,
    @required this.restBaseUrl,
    @required this.xCdataAuthtoken,
  });

  final bool debugMode;
  final String restBaseUrl;
  final String xCdataAuthtoken;

  static AppConfig loadFromEnv() {
    if(kReleaseMode) {
      // prod
      return AppConfig(
        debugMode: false,
        restBaseUrl: 'https://outsourceapi.adcvn.com/api.rsc/NfRpKu',
        xCdataAuthtoken: '8w1I8j8c8X7c8k2J8g5a',
      );
    } else {
      // dev
      return AppConfig(
        debugMode: true,
        restBaseUrl: 'https://outsourceapi.adcvn.com/api.rsc/NfRpKu',
        xCdataAuthtoken: '8w1I8j8c8X7c8k2J8g5a',
      );
    }
  }
}
