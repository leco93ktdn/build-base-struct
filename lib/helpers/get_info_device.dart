import 'dart:io';

import 'package:device_info/device_info.dart';

Future<Map<String, String>> getDeviceInfo() async {
  final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  if (Platform.isIOS) {
    final IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
    final Map<String, String> map = <String, String>{
      'device_id': iosDeviceInfo.identifierForVendor,
      'device_name': iosDeviceInfo.utsname.machine,
    };
    return map;
  }

  final AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
  final Map<String, String> map = <String, String>{
    'device_id': androidDeviceInfo.androidId,
    'device_name': androidDeviceInfo.model,
  };
  return map;
}
