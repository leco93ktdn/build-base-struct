import 'dart:developer';
import 'package:base_struct_redux_app/constants/colors.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';

Future<void> launchURL(String url) async {
  try {
    final bool isLaunch = await canLaunch(url);

    if (!isLaunch) {
      log('Could not launch $url');
      await Fluttertoast.showToast(
          msg: 'Could not launch $url', backgroundColor: accentFourColor);
      return;
    }

    await launch(url);
  } catch (error) {
    print(error);
  }
}
