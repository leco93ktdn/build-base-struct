import 'package:base_struct_redux_app/constants/string.dart';
import 'package:get_storage/get_storage.dart';

class PersistentState {
  PersistentState(this._getStorage)
      : tokenGlobal = _getStorage.read(KeyShared.tokenGlobal),
        phoneNumber = _getStorage.read(KeyShared.phoneNumber),
        authenticationID = _getStorage.read(KeyShared.authenticationID),
        userID = _getStorage.read(KeyShared.userID),
        authenticationKey = _getStorage.read(KeyShared.authenticationKey);

  final GetStorage _getStorage;
  String tokenGlobal;
  String phoneNumber;
  String authenticationID;
  String userID;
  String authenticationKey;

  void setToken({
    String tokenGlobal,
  }) {
    tokenGlobal = tokenGlobal;
    _getStorage.write(KeyShared.tokenGlobal, tokenGlobal);
  }

  void setLoginInfo({
    String phoneNumber,
    String userID,
    String authenticationID,
  }) {
    phoneNumber = phoneNumber;
    userID = userID;
    authenticationID = authenticationID;
    _getStorage.write(KeyShared.phoneNumber, phoneNumber);
    _getStorage.write(KeyShared.authenticationID, authenticationID);
    _getStorage.write(KeyShared.userID, userID);
  }

  void setOTPInfo({
    String authenticationKey,
  }) {
    authenticationKey = authenticationKey;
    _getStorage.write(KeyShared.authenticationKey, authenticationKey);
  }

  void setLogout() {
    tokenGlobal = null;
    phoneNumber = null;
    authenticationID = null;
    userID = null;
    _getStorage.remove(KeyShared.tokenGlobal);
    _getStorage.remove(KeyShared.phoneNumber);
    _getStorage.remove(KeyShared.authenticationID);
    _getStorage.remove(KeyShared.userID);
  }
}
