import 'package:intl/intl.dart';

String dateString(DateTime dateTime) {
  if (dateTime == null) {
    return '-';
  }

  final int day = dateTime.day;
  final int month = dateTime.month;
  final int year = dateTime.year;

  return '${day < 10 ? '0$day' : '$day'}/${month < 10 ? '0$month' : '$month'}/$year';
}

String timeStamptoDateString(String timestamp) {
  if (timestamp == null) {
    return '';
  }

  final DateTime dateTime = DateTime.parse(timestamp);
  return dateString(dateTime);
}

String formatCurrency(int currency) {
  return NumberFormat.currency(locale: 'vi', symbol: 'đ').format(currency).replaceAll('.', ',');
}

String formatByPhoneNumber(String text) {
  if (text == null) {
    return '';
  }

  final String subTextOne = text.substring(0, 4);
  final String subTextTwo = text.substring(4, 7);
  final String subTextThree = text.substring(7, 10);
  final String result = '$subTextOne $subTextTwo $subTextThree';

  return result;
}

String formatTime(int timeNum) {
  if (timeNum == null) {
    return '00';
  }

  return timeNum < 10 ? '0' + timeNum.toString() : timeNum.toString();
}

String greetingByTime() {
  final int hour = DateTime.now().hour;
  if (hour < 12) {
    return 'Chào buổi sáng!';
  }

  if (hour < 17) {
    return 'Chào buổi chiều!';
  }

  return 'Chào buổi tối!';
}
