import 'package:base_struct_redux_app/modules/auth/redux/reducer.dart';
import 'app.state.dart';

AppState appStateReducer(AppState state, dynamic action) {
  return AppState(
    loginState: loginReducer(state.loginState, action),
  );
}
