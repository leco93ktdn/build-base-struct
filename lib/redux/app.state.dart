import 'dart:math';
import 'package:base_struct_redux_app/modules/auth/state/login.state.dart';
import 'package:base_struct_redux_app/modules/home/state/home.state.dart';
import 'package:equatable/equatable.dart';

class AppState extends Equatable {
  final LoginState loginState;
  final HomeState homeState;

  const AppState({this.loginState, this.homeState});

  factory AppState.initial() {
    return AppState(
      loginState: LoginState.initial(),
      homeState: HomeState.initial(),
    );
  }

  AppState copyWith() {
    return AppState(
      loginState: loginState ?? this.loginState,
      homeState: homeState ?? homeState,
    );
  }

  @override
  List<Object> get props => <Object>[loginState, homeState];
}
