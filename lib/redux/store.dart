import 'package:base_struct_redux_app/redux/reducers.dart';
import 'package:redux/redux.dart';
import 'app.state.dart';

final Store<AppState> store = Store<AppState>(appStateReducer,
    initialState: AppState.initial(), middleware: <
        dynamic Function(Store<AppState>, dynamic, dynamic Function(dynamic))>[]);
